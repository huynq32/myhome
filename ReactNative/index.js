import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import App from './App';
// import AppStackNavigator from './src/screens/AppStackNavigator';
import TabNavigator from './src/screens/TabNavigator';


AppRegistry.registerComponent(appName, () => App);
