/* eslint-disable no-undef */
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: Platform.OS === 'ios' ? 30 : 0,
  },

  logoImage:{
    width: 100,
    height: 100,
  },

  button: {
    padding: 10,
    fontSize: 18,
    color: 'white',
    borderRadius: 4,
    backgroundColor: '#81ecec',
  },

  text1: {
    margin: 20,
    fontSize: 15,
  },

  textInput: {
    margin: 10,
    height: 40,
    width: 200,
    padding: 10,
    color: 'black',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#f2f5f6',
  },

  view: {
    flexDirection: 'row',
  },

  button1: {
    width: 200,
    margin: 10,
    padding: 10,
    fontSize: 17,
    color: 'white',
    borderRadius: 4,
    backgroundColor: '#81ecec',
  },

  viewOpa: {
    marginTop: 15,
    alignSelf: 'center',
  },

  textNew: {
    fontSize: 13,
  },

  textNew1: {
    color: 'red',
  },

});
