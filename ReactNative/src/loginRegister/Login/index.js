import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'react-native-button';
import * as firebase from 'react-native-firebase';
import { Text, View, TextInput, Alert, Image, TouchableOpacity } from 'react-native';

// Variables
import styles from './styles';
import ActionTypes from '../../redux/authModule/authAction';
import MYHOME_LOGO from '../../components/resources/images/home.png';


class Login extends Component {
  constructor(props) {
    super(props);
    this.unsubscriber = null;
    this.state = {
      isAuthenticated: false,
      email: '',
      password: '',
      user: null,
    };
  }
  // componentDidMount() {
  //   this.unsubscriber = firebase.auth().onAuthStateChanged((changedUser) => {
  //     this.setState({ user: changedUser });
  //     console.log("Login -> componentDidMount -> changedUser", changedUser)
  //   });
  // }
  componentWillUnmount() {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
  }

  onLogin = async () => {

    try {
      const loggedInUser = await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password);
      this.props.login(loggedInUser);
    } catch (error) {
      console.log(error);
      Alert.alert('Lỗi', 'Thông tin dăng nhập không đúng, vui lòng kiểm tra lại');
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image
            style={styles.logoImage}
            resizeMode="contain"
            source={MYHOME_LOGO} />
        </View>

        <TextInput style={styles.textInput}
          selectTextOnFocus={true}
          keyboardType="email-address"
          placeholder="Login"
          autoCapitalize="none"
          onChangeText={email => this.setState({ email })} />

        <TextInput
          style={styles.textInput}
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={password => this.setState({ password })} />

        <View style={styles.view}>
          <Button style={styles.button1}
            onPress={this.onLogin}>
            Sign in
          </Button>
        </View>

        <TouchableOpacity
          style={styles.viewOpa}
          onPress={() => {
            this.props.navigation.navigate('Register');
          }}>
          <Text style={styles.textNew}>
            Don't have account ? <Text style={styles.textNew1}>Register</Text>
          </Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  login: data => dispatch({ type: ActionTypes.AUTH_LOGIN, data }),
});

export default connect(null, mapDispatchToProps)(Login);
