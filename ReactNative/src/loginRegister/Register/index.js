import React, { Component } from 'react';
import { Text, View, Image, TextInput, Alert, TouchableOpacity } from 'react-native';
import Button from 'react-native-button';

import styles from './styles';
import * as firebase from 'react-native-firebase';
import MYHOME_LOGO from '../../components/resources/images/home.png';

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.unsubscriber = null;
    this.state = {
      isAuthenticated: false,
      email: '',
      password: '',
      user: null,
    };
  }

  onRegister = () => {
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password, this.state.name)
      .then((loggedInUser) => {
        Alert.alert('', 'Đăng ký thành công');
        this.setState({ user: loggedInUser });
      }).catch((error) => {
        Alert.alert('Lỗi', 'Thông tin đăng kí nhập không đúng, vui lòng kiểm tra lại');
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image
            style={styles.logoImage}
            resizeMode="contain"
            source={MYHOME_LOGO} />
          <Text style={styles.text}>
            Resgiter
          </Text>
        </View>

        <TextInput style={styles.textInput}
          keyboardType="email-address"
          placeholder="Email"
          autoCapitalize="none"
          onChangeText={email => this.setState({ email })} />

        <TextInput
          style={styles.textInput}
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={password => this.setState({ password })} />

        <View style={styles.view}>
          <Button style={styles.button1}
            onPress={this.onRegister}>
            Register
          </Button>
        </View>

        <TouchableOpacity
          style={styles.viewOpa}
          onPress={() => {
            this.props.navigation.navigate('Login');
          }}>
          <Text style={styles.textNew}>
            New to My Home ? <Text style={styles.textNew1}>Log in</Text>
          </Text>
        </TouchableOpacity>

      </View>
    );
  }
}
