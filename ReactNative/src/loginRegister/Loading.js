import React from 'react';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';

import ActionTypes from '../redux/authModule/authAction';

class Loading extends React.Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      let loggedInUser = null;

      // Đã đăng nhập trước đó
      if (user) {
        loggedInUser = user;
      }

      this.props.login(loggedInUser);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapDispatchToProps = dispatch => ({
  login: data => dispatch({ type: ActionTypes.AUTH_LOGIN, data }),
});

export default connect(null, mapDispatchToProps)(Loading);
