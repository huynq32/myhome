import axios from 'axios';
export const firebaseAxios = axios.create({
  baseURL: 'https://us-central1-dbonline-56d31.cloudfunctions.net',
  headers:{
    'Content-Type': 'application/json',
  },
});
