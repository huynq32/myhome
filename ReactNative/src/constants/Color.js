export default {
  BLUE: '#0984e3',
  GRAY: '#d5d5d5',
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  SILVER: '#f2f2f2',

  TEXT: '#3C873A',
  ICON: '#3C873A',
  MAINCOLOR: '#81ecec',

  BACKGROUNDCOLOR: '#f2f5f6',
  BACKGROUNDCOLORHOME: '#3C873A',
};
