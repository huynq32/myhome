import { createStore } from 'redux';

// Variables
import rootReducer from './rootReducer';

const store = createStore(rootReducer);

export default store;
