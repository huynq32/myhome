import { combineReducers } from 'redux';

import authReducer from './authModule/authReducer';

export default combineReducers({ authReducer });
