import ActionTypes from '../authModule/authAction';

const defaultState = {
  loading: true,
  loggedInUser: null,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ActionTypes.AUTH_LOGIN:
      return {
        ...state,
        loading: false,
        loggedInUser: action.data,
      };
    case ActionTypes.AUTH_LOGOUT:
      return {
        ...state,
        loggedInUser: null,
      };
    default:
      return state;
  }
};
