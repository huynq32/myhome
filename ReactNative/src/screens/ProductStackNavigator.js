import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Classify from './Classify';
import WareHouse from './WareHouse';
import AddProduct from './AddProduct';
import CheckGoods from './CheckGoods';
import ImportingGoods from './ImportingGoods';
import ListOfProducts from './ListOfProducts';

const Product = createStackNavigator();

export default (props) => {
  // const navigation = useNavigation();

  const route = useRoute();

  // const { route, navigation } = props;

  const { initialRouteName } = route.params;

  return (
    <Product.Navigator initialRouteName={initialRouteName}>
      <Product.Screen name="AddProduct" component={AddProduct} options={{ headerShown: false }} />
      <Product.Screen name="Classify" component={Classify} options={{ headerShown: false }} />
      <Product.Screen name="WareHouse" component={WareHouse} options={{ headerShown: false }} />
      <Product.Screen name="CheckGoods" component={CheckGoods} options={{ headerShown: false }} />
      <Product.Screen name="ListOfProducts" component={ListOfProducts} options={{ headerShown: false }} />
      <Product.Screen name="ImportingGoods" component={ImportingGoods} options={{ headerShown: false }} />
    </Product.Navigator>
  );
};
