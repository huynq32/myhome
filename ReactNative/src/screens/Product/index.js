import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Components

// Variables
import styles from './styles';

export default class Product extends React.Component {

  render = () => {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>

        <View style={styles.viewDepartment}>
          <Text style={styles.textDepartment}>Sản phẩm</Text>
        </View>

        <View style={styles.containerAdd}>
          <TouchableOpacity
            onPress={() => {
              navigate('ProductStackNavigator', { initialRouteName: 'AddProduct' });
            }}>
            <Image
              style={styles.img}
              source={require('../../components/resources/icon/add_green.png')}
            />
            <Text style={styles.textAdd}>Thêm sản phẩm</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.viewList}>
          <TouchableOpacity
            style={styles.viewList1}
            onPress={() => {
              navigate('ProductStackNavigator', { initialRouteName: 'ListOfProducts' });
            }}>
            <MIcon name="calendar-text" size={24} color="#636e72" />
            <Text style={styles.textList}>Danh sách sản phẩm</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.viewList1}
            onPress={() => {
              navigate('ProductStackNavigator', { initialRouteName: 'ImportingGoods' });
            }}>
            <MIcon name="file-document-edit" size={24} color="#636e72" />
            <Text style={styles.textList}>Nhập hàng</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.viewList2}
            onPress={() => {
              navigate('ProductStackNavigator', { initialRouteName: 'CheckGoods' });
            }}>
            <MIcon name="clipboard-check" size={24} color="#636e72" />
            <Text style={styles.textList}>Kiểm hàng</Text>
          </TouchableOpacity>
        </View>


      </View>
    );
  }
}
