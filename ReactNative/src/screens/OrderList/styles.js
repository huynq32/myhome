import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewDepartment: {
    paddingVertical: 10,
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },

  iconTouchable: {
    paddingHorizontal: 10,
  },

  textInputContainer: {
    flex: 1,
    paddingVertical: 0,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  textInput: {
    flex: 1,
  },

  containerAdd: {
    height: 150,
    marginTop: 5,
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    borderColor: Color.WHITE,
    backgroundColor: Color.WHITE,
  },

  textAdd: {
    fontSize: 14,
    color: Color.GRAY,
    textAlign: 'center',
    textAlignVertical: 'center',
  },

  img: {
    width: 50,
    height: 50,
    alignSelf: 'center',
  },

  viewScrollView: {
    marginVertical: 15,
  },

  viewList: {
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: Color.WHITE,
    justifyContent: 'space-between',
  },

  textList: {
    fontSize: 15,
    color: Color.BLACK,
    paddingHorizontal: 10,
    textAlignVertical: 'center',
  },

  textListN: {
    fontSize: 15,
    color: '#0984e3',
    paddingHorizontal: 10,
    textAlignVertical: 'center',
  },

  viewList5: {
    flexDirection: 'row',
  },

  viewPencilMax: {
  },

  viewPencilMin: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },

  viewPencilMinFiled: {
    flex: 1,
    marginLeft: 10,
  },

  viewEnd: {
    padding: 1,
    height: 90,
    width: '100%',
    paddingHorizontal: 10,
    borderColor: Color.WHITE,
    backgroundColor: Color.WHITE,
  },

  viewEnd1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  textViewEnd: {
    fontSize: 18,
    color: Color.BLACK,
    textAlignVertical: 'center',
  },

  viewTouchableOpacity: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  textTouchableOpacity: {
    width: 270,
    height: 40,
    borderRadius: 5,
    textAlign: 'center',
    backgroundColor: '#8fcbf9',
    textAlignVertical: 'center',
  },
  textButton: {
    width: 40,
    height: 40,
    borderRadius: 5,
    alignItems: 'center',
    textAlignVertical: 'center',
    backgroundColor: Color.SILVER,
  },
});
