import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  button: {
    marginBottom: 10,
    backgroundColor: 'blue',
  },
  text: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },

  viewDepartment: {
    padding: 1,
    height: 60,
    width: '100%',
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
    justifyContent: 'space-around',
  },

  textDepartment: {
    fontSize: 18,
  },

  viewCamera: {
    paddingTop: 10,
    alignItems: 'center',
    flexDirection: 'row',
  },

  containerAdd: {
    width: 70,
    height: 70,
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    justifyContent: 'center',
    borderColor: Color.WHITE,
    backgroundColor: Color.WHITE,
  },

  imageView:{
    width: 50,
    height: 50,
  },

  viewuploadAvatar: {
    paddingTop: 10,
  },

  uploadAvatar: {
    width: 70,
    height: 70,
    flexDirection: 'row',
  },

  viewScrollViewImage:{
    flexDirection: 'row',
  },

  viewScrollView: {
    marginVertical: 15,
    paddingHorizontal: 10,
    backgroundColor: Color.WHITE,
  },

  textField: {
    alignItems: 'center',
    flexDirection: 'row',
  },

  viewList: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },


  viewList1: {
    paddingHorizontal: 10,
  },

  viewList2: {
  },

  viewListBar: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
    justifyContent: 'space-between',
  },

  textList: {
    fontSize: 15,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
    textAlignVertical: 'center',
  },

  textListN: {
    marginLeft: 60,
  },

  viewListPrice: {
    height: 50,
    width: '100%',
    flexDirection: 'column',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },

  viewListSlide: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
    justifyContent: 'space-between',
  },
});
