import React from 'react';
import { View, Text, TextInput, ScrollView, TouchableOpacity, Image, TouchableHighlight } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Components
// import { TextField } from 'react-native-material-textfield';

// Variables
import styles from './styles';

export default class CreatingOrders extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      maSKU: '',
      price: '',
      products: '',
      discount: '',
      loading: false,
    });
  }


  render = () => {
    // const { discount } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>

        <View style={styles.viewDepartment}>
          <TouchableHighlight
            style={styles.iconTouchable}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <MIcon name="close" size={24} color="#636e72" />
          </TouchableHighlight>
          <View style={styles.textInputContainer}>
            <MIcon name="magnify" size={24} color="#636e72" />
            <TextInput
              autoCapitalize="none"
              keyboardType="default"
              placeholderTextColor="gray"
              placeholder="Nhập tên, mã SKU, Barcode"
              style={styles.textInput}
              onChangeText={
                (text) => {
                  this.setState({ products: text });
                }
              }
              value={this.state.products}
            />
          </View>
          <TouchableHighlight style={styles.iconTouchable}>
            <MIcon name="dots-vertical" size={24} color="#636e72" />
          </TouchableHighlight>
        </View>

        <View style={styles.containerAdd}>
          <TouchableOpacity
            onPress={() => {
              navigate('AddOrder');
            }}>
            <Image
              style={styles.img}
              source={require('../../components/resources/icon/add-shopping-cart.png')}
            />
            <Text style={styles.textAdd}>Đơn hàng của bạn chưa có sản phẩm nào!</Text>
          </TouchableOpacity>
        </View>

        <ScrollView>

          <View style={styles.viewScrollView}>
            <View style={styles.viewList}>
              <Text style={styles.textList}>Tổng số lượng</Text>
              <Text style={styles.textList}>0</Text>
            </View>
            <View style={styles.viewList}>
              <Text style={styles.textList}>Tổng tiền hàng</Text>
              <Text style={styles.textList}>0</Text>
            </View>


            <View>
              <View>
                <TouchableOpacity style={styles.viewList}>
                  <Text style={styles.textListN}>Chiết khấu</Text>
                  <TextInput
                    maxLength={2}
                    placeholder="0"
                    autoCapitalize="none"
                    keyboardType="numeric"
                    placeholderTextColor="black"
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View>
              <TouchableOpacity
                style={styles.viewList}
                onPress={() => {
                  navigate('DeliveryCharges');
                }}>
                <Text style={styles.textListN}>Phí giao hàng</Text>
                <Text style={styles.textList}>0</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View>
            <View style={styles.viewList}>
              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('AddCustomer');
                }}>
                <MIcon name="account" size={24} color="#636e72" />
                <Text style={styles.textListN}>Thêm khách hàng</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.viewList}>
              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('RetailPrice');
                }}>
                <MIcon name="tag" size={24} color="#636e72" />
                <Text style={styles.textList}>Giá bán lẻ</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.viewScrollView}>
            <View style={styles.viewList}>
              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('PaymentOptions');
                }}>
                <MIcon name="credit-card" size={24} color="#636e72" />
                <Text style={styles.textList}>Chọn phương thức thanh toán</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.viewPencilMax}>
            <View style={styles.viewPencilMin}>
              <MIcon name="pencil" size={24} color="#636e72" />
              <View style={styles.viewPencilMinFiled}>
                <TextInput
                  placeholder="Thêm ghi chú"
                  placeholderTextColor="#3C873A"
                />
              </View>
            </View>
          </View>

        </ScrollView>

        <View style={styles.viewEnd}>
          <View style={styles.viewEnd1}>
            <Text style={styles.textViewEnd}>Tạm tính</Text>
            <Text>0</Text>
          </View>
          <View style={styles.viewTouchableOpacity}>
            <TouchableOpacity>
              <Text style={styles.textTouchableOpacity}>Tạo đơn và giao hàng</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.textButton}>
              <MIcon name="dots-horizontal" size={24} color="#636e72" />
            </TouchableOpacity>
          </View>
        </View>

      </View>
    );
  }
}
