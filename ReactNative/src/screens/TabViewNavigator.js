import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Sales from './Sales';
import Store from './Store';
import Financial from './Financial';

const TabView = createMaterialTopTabNavigator();

const TabViewNavigator = () => (
  <TabView.Navigator>
    <TabView.Screen name="BÁN HÀNG" component={Sales} />
    <TabView.Screen name="KHO" component={Store} />
    <TabView.Screen name="TÀI CHÍNH" component={Financial} />
  </TabView.Navigator>
);

export default TabViewNavigator;
