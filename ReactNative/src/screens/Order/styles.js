import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewDepartment: {
    justifyContent: 'flex-start',
  },


  textDepartment: {
    padding: 1,
    height: 50,
    width: '100%',
    fontSize: 18,
    borderWidth: 1,
    color: Color.BLACK,
    textAlign: 'center',
    borderColor: Color.WHITE,
    textAlignVertical: 'center',
    backgroundColor: Color.WHITE,
  },

  containerAdd: {
    width: 250,
    height: 150,
    marginTop: 35,
    alignSelf: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    justifyContent: 'center',
    borderColor: Color.WHITE,
    backgroundColor: Color.WHITE,
  },

  textAdd: {
    fontSize: 18,
    textAlign: 'center',
    textAlignVertical: 'center',
  },

  img: {
    width: 50,
    height: 50,
    alignSelf: 'center',
  },

  view3: {
    flex: 1,
    paddingVertical: 33,
  },

  viewList: {
    borderRadius: 10,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: Color.WHITE,
  },

  viewList1: {
    paddingVertical: 10,
    flexDirection:'row',
    borderBottomWidth: 1,
    borderColor: Color.GRAY,
  },

  viewList2: {
    paddingVertical: 10,
    flexDirection:'row',
  },

  textList: {
    fontSize: 18,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
    textAlignVertical: 'center',
  },

});
