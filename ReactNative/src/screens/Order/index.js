import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Components

// Variables
import styles from './styles';

export default class Order extends React.Component {

  render = () => {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>

        <View style={styles.viewDepartment}>
          <Text style={styles.textDepartment}>Đơn hàng </Text>
        </View>

        <View style={styles.containerAdd}>
          <TouchableOpacity
            onPress={() => {
              navigate('OrderStackNavigator', { initialRouteName: 'CreatingOrders' });
            }}>
            <Image
              style={styles.img}
              source={require('../../components/resources/icon/add_green.png')}
            />
            <Text style={styles.textAdd}>Tạo đơn và giao hàng</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.viewList}>
          <TouchableOpacity
            style={styles.viewList1}
            onPress={() => {
              navigate('OrderStackNavigator', { initialRouteName: 'OrderList' });
            }}>
            <MIcon name="calendar-text" size={24} color="#636e72" />
            <Text style={styles.textList}>Danh sách đơn hàng</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewList1}
            onPress={() => {
              navigate('OrderStackNavigator', { initialRouteName: 'CustomerReturn' });
            }}>
            <MIcon name="file-undo" size={24} color="#636e72" />
            <Text style={styles.textList}>Khách trả hàng</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewList1}
            onPress={() => {
              navigate('OrderStackNavigator', { initialRouteName: 'DeliveryManager' });
            }}>
            <MIcon name="truck-check" size={24} color="#636e72" />
            <Text style={styles.textList}>Quản lí giao hàng</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewList2}
            onPress={() => {
              navigate('OrderStackNavigator', { initialRouteName: 'ShippingPartner' });
            }}>
            <MIcon name="truck-fast" size={24} color="#636e72" />
            <Text style={styles.textList}>Đối tác vận chuyển</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}
