import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.WHITE,
  },

  viewRenderBackground: {
    marginVertical: 10,
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    backgroundColor: Color.WHITE,
  },

  viewRenderForeground: {
    width: 350,
    paddingVertical: 10,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: Color.WHITE,
  },

  icon: {
    paddingHorizontal: 10,
  },

  textInput: {
    flex: 1,
    borderRadius: 10,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewBarcode: {
    width: 35,
    borderLeftWidth: 1,
    alignItems: 'flex-end',
    borderColor: Color.GRAY,
  },

  viewTextHeader: {
  },

  textHeader: {
    fontSize: 18,
    textAlign: 'center',
  },

  viewScorllView: {
    paddingHorizontal: 10,
  },

  textScorllView: {
    fontSize: 15,
  },
});
