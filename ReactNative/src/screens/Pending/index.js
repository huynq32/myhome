import React from 'react';
import { View, TextInput, TouchableHighlight, Text } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
// Components

// Variables
import styles from './styles';

export default class Pending extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      maSKU: '',
      price: '',
      products: '',
      discount: '',
      loading: false,
    });
  }


  render = () => {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.viewRenderBackground}>
          <TouchableHighlight
            style={styles.icon}
            onPress={() => {
              navigation.goBack();
            }}>
            <MIcon name="arrow-left" size={24} color="#636e72" />
          </TouchableHighlight>
          <View style={styles.viewTextHeader}>
            <Text style={styles.textHeader}>Chờ duyệt</Text>
          </View>
          <View>
            <MIcon name="dots-vertical" size={24} color="#636e72" />
          </View>
        </View>
        <ParallaxScrollView
          backgroundColor="white"
          stickyHeaderHeight={10}
          parallaxHeaderHeight={10}
          contentBackgroundColor="white"
          renderContentBackground={() => (
            <View style={styles.viewRenderForeground}>
              <View style={styles.textInput}>
                <TextInput
                  autoCapitalize="none"
                  keyboardType="default"
                  placeholderTextColor="gray"
                  placeholder="Nhập tên, số điện thoại , mã"
                  style={styles.textInput}
                  onChangeText={
                    (text) => {
                      this.setState({ products: text });
                    }
                  }
                  value={this.state.products}
                />
                <View style={styles.viewBarcode}>
                  <MIcon name="barcode-scan" size={24} color="#636e72" />
                </View>
              </View>
              <TouchableHighlight style={styles.icon}>
                <MIcon name="magnify" size={24} color="#636e72" />
              </TouchableHighlight>
            </View>
          )}>
          <View style={styles.viewScorllView}>
            <Text style={styles.textScorllView}>
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
              A callback function that is invoked when the parallax header is hidden or shown (as the user is scrolling). Function is called with a boolean value to indicate whether header is visible or not.
          </Text>
          </View>
        </ParallaxScrollView>
      </View>

    );
  }
}
