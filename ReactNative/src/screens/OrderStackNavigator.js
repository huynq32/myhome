import React from 'react';
import { useRoute } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AddOrder from './AddOrder';
import OrderList from './OrderList';
import AddCustomer from './AddCustomer';
import RetailPrice from './RetailPrice';
import CustomerReturn from './CustomerReturn';
import PaymentOptions from './PaymentOptions';
import CreatingOrders from './CreatingOrders';
import ShippingPartner from './ShippingPartner';
import DeliveryManager from './DeliveryManager';
import DeliveryCharges from './DeliveryCharges';


const Order = createStackNavigator();

export default () => {

  const route = useRoute();

  const { initialRouteName } = route.params;
  return (
    <Order.Navigator initialRouteName={initialRouteName}>
      <Order.Screen name="CreatingOrders" component={CreatingOrders} options={{ headerShown: false }} />
      <Order.Screen name="AddOrder" component={AddOrder} options={{ headerShown: false }} />
      <Order.Screen name="OrderList" component={OrderList} options={{ headerShown: false }} />
      <Order.Screen name="AddCustomer" component={AddCustomer} options={{ headerShown: false }} />
      <Order.Screen name="RetailPrice" component={RetailPrice} options={{ headerShown: false }} />
      <Order.Screen name="PaymentOptions" component={PaymentOptions} options={{ headerShown: false }} />
      <Order.Screen name="CustomerReturn" component={CustomerReturn} options={{ headerShown: false }} />
      <Order.Screen name="DeliveryManager" component={DeliveryManager} options={{ headerShown: false }} />
      <Order.Screen name="ShippingPartner" component={ShippingPartner} options={{ headerShown: false }} />
      <Order.Screen name="DeliveryCharges" component={DeliveryCharges} options={{ headerShown: false }} />

    </Order.Navigator>
  );
};
