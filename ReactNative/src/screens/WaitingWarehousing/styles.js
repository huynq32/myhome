import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';
import { Platform } from 'react-native';

export default StyleSheet.create({

  container: {
    flex: 1,
  },

  containerProduct: {
    flex: 1,
    flexDirection: 'row',
  },

  viewDepartment: {
    borderWidth: 1,
    paddingVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
    // justifyContent: 'space-between',
  },

  iconTouchable: {
    paddingHorizontal: 10,
  },

  textView: {
    fontSize: 18,
    textAlign: 'center',
  },

  fadingContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },

  textInput: {
    flex: 1,
    // width: 250,
    // height: 50,
    borderWidth: 1,
    marginRight: 20,
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    borderColor: Color.BACKGROUNDCOLOR,
    backgroundColor: Color.BACKGROUNDCOLOR,
    paddingVertical: Platform.OS === 'ios' ? undefined : 5,
  },

});
