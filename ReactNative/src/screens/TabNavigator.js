import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './Home';
import Order from './Order';
import Notify from './Notify';
import Profile from './Profile';
import Products from './Product';
import CloudFirestore from '../components/CloudFirestore';

const BottomTab = createBottomTabNavigator();

const Configs = {
	Home: {
		screen: Home,
		navigationOptions: {
			title: 'Tổng quan',
			tabBarIcon: ({ color }) => <MIcon name="home" size={24} color={color} />,
		},
	},
	Order: {
		screen: Order,
		navigationOptions: {
			title: 'Đơn hàng',
			tabBarIcon: ({ color }) => <MIcon name="calendar-text" size={24} color={color} />,
		},
	},
	Products: {
		screen: Products,
		navigationOptions: {
			title: 'Sản phẩm',
			tabBarIcon: ({ color }) => <MIcon name="account" size={24} color={color} />,
		},
	},
	Notify: {
		screen: Notify,
		navigationOptions: {
			title: 'Báo cáo',
			tabBarIcon: ({ color }) => <MIcon name="chart-areaspline" size={24} color={color} />,
		},
	},
	// Profile: {
	// 	screen: Profile,
	// 	navigationOptions: {
	// 		title: 'Thêm',
	// 		tabBarIcon: ({ color }) => <MIcon name="apps" size={24} color={color} />,
	// 	},
	// },
	CloudFirestore: {
		screen: CloudFirestore,
		navigationOptions: {
			title: 'Thêm',
			tabBarIcon: ({ color }) => <MIcon name="apps" size={24} color={color} />,
		},
	},
};

export default () => (
	<BottomTab.Navigator
		tabBarOptions={{
			activeTintColor: '#3C873A',
			inactiveTintColor: 'black',
		}}>
		<BottomTab.Screen name="Home" component={Home} options={Configs.Home.navigationOptions} />
		<BottomTab.Screen name="Order" component={Order} options={Configs.Order.navigationOptions} />
		<BottomTab.Screen name="Products" component={Products} options={Configs.Products.navigationOptions} />
		<BottomTab.Screen name="Notify" component={Notify} options={Configs.Notify.navigationOptions} />
		<BottomTab.Screen name="CloudFirestore" component={CloudFirestore} options={Configs.CloudFirestore.navigationOptions} />
	</BottomTab.Navigator>
);
