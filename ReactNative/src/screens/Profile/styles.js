import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex:1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewDepartment:{
    justifyContent: 'flex-start',
  },

  textDepartment:{
    padding: 1,
    height: 50,
    width: '100%',
    fontSize: 18,
    borderWidth: 1,
    color: Color.BLACK,
    textAlign: 'center',
    borderColor: Color.WHITE,
    textAlignVertical: 'center',
    backgroundColor: Color.WHITE,
  },


  viewContainer:{
    flex:1,
    paddingVertical: 33,
  },

  viewGeneralMin: {
    flex: 1,
    marginTop: 10,
    borderRadius: 10,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: Color.WHITE,
  },

  viewList: {
    paddingVertical: 10,
    flexDirection:'row',
    borderBottomWidth: 1,
    borderColor: Color.GRAY,
  },

  viewList1: {
    paddingVertical: 10,
    flexDirection:'row',
  },

  textList: {
    fontSize: 18,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
  },

  textViewEnd:{
    padding: 1,
    height: 40,
    color: 'red',
    width: '100%',
    borderWidth: 1,
    textAlign: 'center',
    borderColor: Color.WHITE,
    textAlignVertical: 'center',
    backgroundColor: Color.WHITE,
  },

});
