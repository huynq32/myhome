import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';


import styles from './styles';
import firebase from 'react-native-firebase';
import ActionTypes from '../../redux/authModule/authAction';


class Profile extends Component {
  state = {
    email: '',
  };

  componentDidMount() {
    const { email } = firebase.auth().currentUser;
    this.setState({ email });
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.viewDepartment}>
          <Text style={styles.textDepartment}>{this.state.email}</Text>
        </View>

        <View style={styles.viewContainer}>
          <ScrollView>
            <View style={styles.viewGeneralMin}>
              <View style={styles.viewList}>
                <MIcon name="account" size={24} color="#636e72" />
                <Text style={styles.textList}>Khách hàng</Text>
              </View>
              <View style={styles.viewList}>
                <MIcon name="cash-100" size={24} color="#636e72" />
                <Text style={styles.textList}>Sổ quỹ</Text>
              </View>
              <View style={styles.viewList}>
                <MIcon name="bell" size={24} color="#636e72" />
                <Text style={styles.textList}>Thông báo</Text>
              </View>
              <View style={styles.viewList}>
                <MIcon name="ticket-percent" size={24} color="#636e72" />
                <Text style={styles.textList}>Ưu dãi của tôi</Text>
                <MIcon name="new-box" size={20} color="red" />
              </View>
              <View style={styles.viewList}>
                <MIcon name="settings" size={24} color="#636e72" />
                <Text style={styles.textList}>Cấu hình</Text>
              </View>
              <View style={styles.viewList}>
                <MIcon name="lifebuoy" size={24} color="#636e72" />
                <Text style={styles.textList}>Hỗ trợ</Text>
              </View>
              <View style={styles.viewList1}>
                <MIcon name="gift-outline" size={24} color="#636e72" />
                <Text style={styles.textList}>Giới thiệu khách hàng</Text>
              </View>
            </View>
          </ScrollView>
        </View>

        <View>
          <TouchableOpacity
            onPress={() => {
              this.props.logout('');
            }}>
            <Text style={styles.textViewEnd}>ĐĂNG XUẤT TÀI KHOẢN</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch({ type: ActionTypes.AUTH_LOGOUT }),
});

export default connect(null, mapDispatchToProps)(Profile);
