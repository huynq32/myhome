import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewGeneralMin: {
    flex: 1,
    marginTop: 10,
    borderRadius: 10,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: Color.WHITE,
  },

  viewList: {
    paddingVertical: 10,
    flexDirection:'row',
    borderBottomWidth: 1,
    borderColor: Color.GRAY,
  },

  viewList1: {
    paddingVertical: 10,
    flexDirection:'row',
  },

  textList: {
    fontSize: 18,
    marginTop: 10,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
  },

});
