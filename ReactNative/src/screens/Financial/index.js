import React from 'react';
import { View, Text, ScrollView, TouchableHighlight } from 'react-native';

// Components

// Variables
import styles from './styles';

export default class Financial extends React.Component {

  render = () => {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.viewGeneralMin}>
            <View style={styles.viewList}>
              <TouchableHighlight>
                <Text style={styles.textList}>Sổ quỹ</Text>
              </TouchableHighlight>
            </View>
            <View style={styles.viewList}>
              <Text style={styles.textList}>Báo cáo lãi lỗ</Text>
            </View>
            <View style={styles.viewList}>
              <Text style={styles.textList}>Báo cáo công nợ phải thu</Text>
            </View>
            <View style={styles.viewList1}>
              <Text style={styles.textList}>Báo cáo công nợ phải trả</Text>
            </View>
          </View>
        </ScrollView>

      </View>
    );
  }
}
