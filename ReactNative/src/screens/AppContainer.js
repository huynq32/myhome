import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';

// Components
import Loading from '../loginRegister/Loading';
import AppStackNavigator from './AppStackNavigator';
import AuthStackNavigator from './AuthStackNavigator';

const AppContainer = () => {
  // React hook

  const authReducer = useSelector(rootState => rootState.authReducer);

  const isLoadingVisible = authReducer.loading === true;

  const isLoggedIn = authReducer.loggedInUser !== null;

  return (
    <NavigationContainer>
      {isLoadingVisible && <Loading />}
      {isLoadingVisible === false && (
        <React.Fragment>
          {isLoggedIn === false && <AuthStackNavigator />}
          {isLoggedIn === true && <AppStackNavigator />}
        </React.Fragment>
      )}
    </NavigationContainer>
  );
};

export default AppContainer;
