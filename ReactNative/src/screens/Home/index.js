import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';

// Components
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Variables
import styles from './styles';
import firebase from 'react-native-firebase';

const parentList = [];

for (let i = 0; i < 2; i++) {
  const childList = [
    { number: '1', name: 'Khách mới', percent: '0%' },
    { number: '2', name: 'Doanh thu', percent: '0%' },
    { number: '3', name: 'Đơn hoàn thành', percent: '0%' },

  ];

  parentList.push({ childList });
}

export default class Home extends Component {
  state = {
    email: '',
  };

  componentDidMount() {
    const { email } = firebase.auth().currentUser;
    this.setState({ email });
  }

  renderParentItem = (item, index) => {
    const isLastItem = index === parentList.length - 1;

    const customStyles = {
      container: { flexDirection: 'row' },
    };

    if (isLastItem === false) {
      customStyles.container.marginBottom = 10;
    }

    return (
      <View key={index} style={customStyles.container}>
        {item.childList.map((childItem, childIndex) => this.renderChildItem(childItem, childIndex, item.childList))}
      </View>
    );
  }

  renderChildItem = (childItem, childIndex, currentList) => {
    const isLastItem = childIndex === currentList.length - 1;

    const customStyles = {
      container: {},
    };

    if (isLastItem === false) {
      customStyles.container.marginRight = 10;
    }

    return (
      <View key={childIndex} style={[styles.viewGeneralMin, customStyles.container]}>
        <Text style={styles.textGeneral}>{childItem.number}</Text>
        <Text style={styles.textGeneral}>{childItem.name}</Text>
        <Text style={styles.textGeneral}>{childItem.percent}</Text>
      </View>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.container, styles.imageBackground]}>
          <View style={styles.containerSize} />
          <View style={styles.container}>
            <View style={styles.viewDepartment}>
              <View style={styles.hi}>
                <Text style={styles.textDepartment}>
                  Hi {this.state.email} !
                </Text>
              </View>
              <View style={styles.mIcon}>
                <MIcon name="bell" size={24} color="white" />
              </View>
            </View>

            <View style={styles.viewSales}>
              <View>
                <Text style={styles.textSales}>
                  DOANH THU
                </Text>
              </View>

              <View style={styles.textView21}>
                <Text style={styles.textText21}>
                  Chưa có dữ liệu
                </Text>
              </View>
            </View>

            <View>
              <Text style={styles.title1}>
                TỔNG QUAN TRONG NGÀY
              </Text>
            </View>

            <View style={styles.viewGeneral}>
              {parentList.map(this.renderParentItem)}
            </View>

            <View>
              <Text style={styles.title1}>
                ĐƠN HÀNG CHỞ XỬ LÝ
              </Text>
            </View>

            <View style={styles.viewOrdersPendingT}>
              <TouchableOpacity
                style={styles.viewOrdersPending}
                onPress={() => {
                  navigate('HomeStackNavigator', { initialRouteName: 'Pending' });
                }}>
                <MIcon name="calendar-multiple-check" size={24} color="#3C873A" />
                <Text style={styles.textOrdersPending}>Chờ duyệt</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.viewOrdersPending}
                onPress={() => {
                  navigate('HomeStackNavigator', { initialRouteName: 'PayAwait' });
                }}>
                <MIcon name="credit-card" size={24} color="#3C873A" />
                <Text style={styles.textOrdersPending}>Chờ thanh toán</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.viewOrdersPending}
                onPress={() => {
                  navigate('HomeStackNavigator', { initialRouteName: 'WaitingWarehousing' });
                }}>
                <MIcon name="garage" size={24} color="#3C873A" />
                <Text style={styles.textOrdersPending}>Chờ xuất kho</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.viewOrdersPending}
                onPress={() => {
                  navigate('HomeStackNavigator', { initialRouteName: 'WaitingWarehousing' });
                }}>
                <MIcon name="truck-delivery" size={24} color="#3C873A" />
                <Text style={styles.textOrdersPending}>Đang giao hàng</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.viewOrdersPending1}
                onPress={() => {
                  navigate('HomeStackNavigator', { initialRouteName: 'WaitingWarehousing' });
                }}>
                <MIcon name="file-undo" size={24} color="#3C873A" />
                <Text style={styles.textOrdersPending}>Chờ nhận hàng hủy</Text>
              </TouchableOpacity>
            </View>

            <View>
              <Text style={styles.title1}>
                THÔNG TIN KHO
              </Text>
            </View>

            <View style={styles.viewOrdersPendingT}>
              <View style={styles.viewOrdersPending}>
                <Text style={styles.textWarehouse}>Sản phẩm dưới định mức tồn</Text>
              </View>
              <View style={styles.viewOrdersPending}>
                <Text style={styles.textWarehouse}>Số lượng tồn kho</Text>
              </View>
              <View style={styles.viewOrdersPending1}>
                <Text style={styles.textWarehouse}>Giá trị tồn kho</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView >

    );
  }
}
