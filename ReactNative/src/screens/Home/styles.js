import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({
  container: {
    flex: 1,
  },

  imageBackground: {
    paddingHorizontal: 20,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  containerSize: {
    left: 0,
    right: 0,
    height: 160,
    position: 'absolute',
    borderBottomLeftRadius: 35,
    borderBottomRightRadius: 35,
    backgroundColor: Color.BACKGROUNDCOLORHOME,
  },

  viewDepartment: {
    padding: 1,
    height: 60,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  textDepartment: {
    fontSize: 18,
    color: Color.WHITE,
    textAlign: 'center',
    textAlignVertical: 'center',
  },

  hi: {
    flex: 1,
  },

  mIcon: {
    alignItems: 'flex-end',
  },

  viewSales: {
    height: 260,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },

  textSales: {
    fontSize: 18,
    marginLeft: 10,
    color: Color.BLACK,
  },

  // co data roi xoa
  textView21: {
    alignSelf: 'center',
  },
  // co data roi xoa
  textText21: {
    textAlign: 'center',
    fontSize: 18,
    color: Color.BLACK,
  },

  title1: {
    fontSize: 18,
    color: Color.BLACK,
    paddingVertical: 10,
  },

  viewGeneral: {
    padding: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Color.GRAY,
    backgroundColor: 'white',
  },

  viewGeneralMin: {
    flex: 1,
    padding: 10,
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Color.WHITE,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  textGeneral: {
    marginVertical: 2,
    textAlign: 'center',
  },

  viewOrdersPendingT: {
    flex: 1,
    marginTop: 10,
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    borderColor: Color.GRAY,
    justifyContent: 'center',
    backgroundColor: Color.WHITE,
  },

  viewOrdersPending: {
    paddingVertical: 10,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: Color.GRAY,
  },

  viewOrdersPending1: {
    paddingVertical: 10,
    flexDirection: 'row',
  },

  textOrdersPending: {
    fontSize: 17,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
  },

  textWarehouse: {
    fontSize: 17,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
    textAlignVertical: 'center',
  },

});
