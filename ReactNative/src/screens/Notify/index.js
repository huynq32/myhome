import React from 'react';
import { Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';



// Components
import TabViewNavigator from '../TabViewNavigator';

// Variables
import styles from './styles';

export default class Notify extends React.Component {
  render = () => {
    return (
      <NavigationContainer
        independent={true}>
        <Text style={styles.textDeparman}>Báo cáo</Text>
        <TabViewNavigator />
      </NavigationContainer>

    );
  }
}
