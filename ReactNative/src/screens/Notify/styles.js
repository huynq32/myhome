import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({
  container: {
    flex: 1,
  },

  textDeparman: {
    fontSize: 25,
    textAlign: 'center',
    color: Color.BLACK,
    backgroundColor: Color.WHITE,
  },
});
