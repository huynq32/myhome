import React from 'react';
import { View, Text, TouchableOpacity, TouchableHighlight } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Components

// Variables
import styles from './styles';

export default class DeliveryCharges extends React.Component {
  render = () => {
    const { navigation } = this.props;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.viewDepartment}>

          <TouchableHighlight
            onPress={() => {
              navigation.goBack();
            }}>
            <MIcon name="arrow-left" size={24} />
          </TouchableHighlight>
          <View style={styles.view}>
            <Text style={styles.textDepartment}>
              Thêm sản phẩm
            </Text>
          </View>

        </View>

        <View>
          <Text style={styles.text}>
            Phí cửa hàng thu của khách hàng
          </Text>
        </View>

        <View>
            <View style={styles.viewList}>
              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('');
                }}>
                <MIcon name="cash-usd" size={24} color="#636e72" />
                <Text style={styles.textListN}>Miễn phí giao hàng</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.viewList}>
              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('');
                }}>
                <MIcon name="truck-fast" size={24} color="#636e72" />
                <Text style={styles.textList}>Phí dự kiến của đối tác vận chuyển</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    );
  }
}
