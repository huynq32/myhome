import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewDepartment: {
    padding: 1,
    height: 60,
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },

  view:{
    flex: 1,
  },

  textDepartment: {
    fontSize: 18,
    textAlign: 'center',
  },

  text: {
    textAlign: 'center',
    marginVertical: 10,
  },

  viewList: {
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: Color.WHITE,
    justifyContent: 'space-between',
  },

  viewList5: {
    flexDirection: 'row',
  },

  textList: {
    fontSize: 15,
    color: Color.BLACK,
    paddingHorizontal: 10,
    textAlignVertical: 'center',
  },

  textListN: {
    fontSize: 15,
    color: '#0984e3',
    paddingHorizontal: 10,
    textAlignVertical: 'center',
  },
});
