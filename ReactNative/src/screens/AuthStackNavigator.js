import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../loginRegister/Login';
import Register from '../loginRegister/Register';

const AuStack = createStackNavigator();

export default () => (
  <AuStack.Navigator>
    <AuStack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
    <AuStack.Screen name="Register" component={Register} options={{ headerShown: false }}/>
  </AuStack.Navigator>
);
