import React from 'react';
import { useRoute } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Pending from './Pending';
import PayAwait from './PayAwait';
import WaitingWarehousing from './WaitingWarehousing';

const Home = createStackNavigator();

export default (props) => {
  // const navigation = useNavigation();

  const route = useRoute();

  // const { route, navigation } = props;

  const { initialRouteName } = route.params;

  return (
    <Home.Navigator initialRouteName={initialRouteName}>
      <Home.Screen name="Pending" component={Pending} options={{ headerShown: false }} />
      <Home.Screen name="PayAwait" component={PayAwait} options={{ headerShown: false }} />
      <Home.Screen name="WaitingWarehousing" component={WaitingWarehousing} options={{ headerShown: false }} />
    </Home.Navigator>
  );
};
