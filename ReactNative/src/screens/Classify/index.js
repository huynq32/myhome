import React from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, TouchableHighlight, Switch } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import { TextField } from 'react-native-material-textfield';

// Components
import Color from '../../constants/Color';
// Variables
import styles from './styles';

export default class WareHouse extends React.Component {

  onChangeText = (text) => {
    const stateModel = { retailPrice: this.state.retailPrice };

    const notNumbericRegex = /\D+/gm;

    const isOnlyNumber = notNumbericRegex.test(text) === false;

    if (isOnlyNumber) {
      stateModel.retailPrice = text;
    }

    this.setState(stateModel);
  }

  render = () => {
    return (
      <View style={styles.container}>
        <View style={styles.viewDepartment}>

          <TouchableHighlight
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <MIcon name="close" size={24} color="#636e72" />
          </TouchableHighlight>
          <Text style={styles.textDepartment}>
            Phân loại
          </Text>
          <TouchableHighlight>
            <MIcon name="check" size={24} color="#636e72" />
          </TouchableHighlight>

        </View>

        <View style={styles.viewScrollView}>
          <View style={styles.viewList1}>
            <View style={styles.textField}>

              <View style={{ flex: 1, marginRight: 10 }}>
                <TextField
                  placeholder="0"
                  label="Giá bán lẻ"
                  autoCapitalize="none"
                  keyboardType="numeric"
                  placeholderTextColor="black"
                  onChangeText={this.onChangeText}
                // value={this.state.newPrice}
                />
              </View>
              <View style={{ flex: 1 }}>
                <TextField
                  placeholder="0"
                  label="Giá bán buôn"
                  autoCapitalize="none"
                  keyboardType="numeric"
                  placeholderTextColor="black"
                  onChangeText={this.onChangeText}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
