import React from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, TouchableHighlight, Switch } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import { TextField } from 'react-native-material-textfield';
import ImagePicker from 'react-native-image-crop-picker';

// Components
// import Color from '../../constants/Color';
// Variables
import styles from './styles';

export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      images: null,
      retailPrice: '',
      switchValueTax: true,
      switchValueAllow: true,
    };
  }

  pickMultiple() {
    ImagePicker.openPicker({
      maxFiles: 5,
      multiple: true,
      forceJpg: true,
      sortOrder: 'desc',
      includeExif: true,
      waitAnimationEnd: false,
    }).then(images => {
      this.setState({
        image: null,
        images: images.map(i => {
          return { uri: i.path, width: i.width, height: i.height, mime: i.mime };
        }),
      });
    }).catch(e => alert(e));
  }

  renderImage(image) {
    return <Image
      source={image}
      resizeMode='contain'
      style={styles.imageView}
    />;
  }

  renderAsset(image) {
    if (image.mime) {
      return this.renderImage(image);
    }
  }


  switchAllow = value => {
    this.setState({ switchValueAllow: value });
  }

  switchTax = value => {
    this.setState({ switchValueTax: value });
  }

  onChangeText = (text) => {
    const stateModel = { retailPrice: this.state.retailPrice };

    const notNumbericRegex = /\D+/gm;

    const isOnlyNumber = notNumbericRegex.test(text) === false;

    if (isOnlyNumber) {
      stateModel.retailPrice = text;
    }

    this.setState(stateModel);
  }

  render = () => {
    const { images } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.viewDepartment}>

          <TouchableHighlight
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <MIcon name="close" size={24} color="#636e72" />
          </TouchableHighlight>
          <Text style={styles.textDepartment}>
            Thêm sản phẩm
          </Text>
          <TouchableHighlight>
            <MIcon name="check" size={24} color="#636e72" />
          </TouchableHighlight>

        </View>
        <View style={styles.viewCamera}>

          <View style={styles.containerAdd}>
            <TouchableHighlight
              onPress={this.pickMultiple.bind(this)}>
              <MIcon name="camera" size={24} color="#636e72" />
            </TouchableHighlight>
          </View>

          <ScrollView>
            <View style={styles.viewScrollViewImage}>
              {images ? images.map(i =>
                <View key={i.uri}>{this.renderAsset(i)}</View>) : null}
            </View>
          </ScrollView>

        </View>

        <ScrollView>

          <View style={styles.viewScrollView}>
            <View style={styles.viewList1}>
              <TextField
                label="Tên sản phẩm"
              />
              <TextField
                label="Mã sản phẩm / SKU"
              />
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ flex: 1 }}>
                  <TextField
                    label="Barcode"
                  />
                </View>
                <MIcon name="barcode-scan" size={24} color="#3C873A" />
              </View>

              <TextField
                label="Mô tả"
              />

              <View style={styles.textField}>

                <View style={{ flex: 1, marginRight: 10 }}>
                  <TextField
                    placeholder="0"
                    label="Giá bán lẻ"
                    autoCapitalize="none"
                    keyboardType="numeric"
                    placeholderTextColor="black"
                    onChangeText={this.onChangeText}
                  // value={this.state.newPrice}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <TextField
                    placeholder="0"
                    label="Giá bán buôn"
                    autoCapitalize="none"
                    keyboardType="numeric"
                    placeholderTextColor="black"
                    onChangeText={this.onChangeText}
                  />
                </View>

              </View>

              <TextField
                label="Giá nhập"
                placeholder="0"
                autoCapitalize="none"
                keyboardType="numeric"
                placeholderTextColor="black"
                onChangeText={this.onChangeText}
              />
            </View>
          </View>

          <View style={styles.viewScrollView}>
            <View style={styles.viewList}>

              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('WareHouse');
                }}>
                <View>
                  <MIcon name="garage" size={24} color="#636e72" />
                </View>
                <View>
                  <Text style={styles.textList}>Kho hàng</Text>
                  <Text style={styles.textList}>Đơn vị, khối lượng, tồn kho</Text>
                </View>
              </TouchableOpacity>

            </View>

            <View style={styles.viewList}>

              <TouchableOpacity
                style={styles.viewList5}
                onPress={() => {
                  navigate('Classify');
                }}>
                <View>
                  <MIcon name="format-list-bulleted-type" size={24} color="#636e72" />
                </View>
                <View>
                  <Text style={styles.textList}>Phân loại</Text>
                  <Text style={styles.textList}>Loại sản phẩm, nhãn hiệu, thẻ Tags</Text>
                </View>
                {/* <View>
                <MIcon name="chevron-right" size={24} color="gray" />
              </View> */}
              </TouchableOpacity>

            </View>
          </View>

          <View style={styles.viewScrollView}>
            <View style={styles.viewListSlide}>
              <Text style={styles.textList}>Áp dụng thuế</Text>
              <Switch
                onValueChange={this.switchTax}
                value={this.state.switchValueTax}
              />
            </View>
            <View style={styles.viewListSlide}>
              <Text style={styles.textList}>Cho phép bán</Text>
              <Switch
                onValueChange={this.switchAllow}
                value={this.state.switchValueAllow}
              />
            </View>
          </View>

        </ScrollView>
      </View >
    );
  }
}
