import React from 'react';
import { Text, Animated, TextInput, View, Dimensions, TouchableHighlight } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';


// Components

// Variables
import styles from './styles';
import Color from '../../constants/Color';

export default class ListOfProducts extends React.Component {
  fadeAnim = new Animated.Value(0);
  refInput = React.createRef();

  fadeIn = () => {
    Animated.timing(this.fadeAnim, {
      toValue: 1,
      duration: 500,
    }).start(()=>{
      this.refInput.current.focus();
    });
  };

  fadeOut = () => {
    Animated.timing(this.fadeAnim, {
      toValue: 0,
      duration: 500,
    }).start(()=>{
      this.refInput.current.blur();
    });
  };


  render() {
    const { width } = Dimensions.get('window');

    const customStyles = {
      titleContainer: {
        backgroundColor: 'white',
      },
      searchContainer: {
        right: 0,
        position: 'absolute',
        opacity: this.fadeAnim,
        zIndex: this.fadeAnim.interpolate({ inputRange: [0, 0.01, 1], outputRange: [-1, 1, 1] }),
        left: this.fadeAnim.interpolate({ inputRange: [0, 1], outputRange: [width, 0] }),
        width: this.fadeAnim.interpolate({ inputRange: [0, 1], outputRange: ['0%', '100%'] }),
      },
    };

    return (
      <View style={[styles.container, { backgroundColor: Color.WHITE }]}>

        <View style={styles.viewDepartment}>
          <TouchableHighlight
            style={styles.iconTouchable}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <MIcon name="arrow-left" size={24} color="#636e72" />
          </TouchableHighlight>

          <View style={styles.containerProduct}>
            <View style={[styles.container, customStyles.titleContainer]}>
              <Text style={styles.textView}>
                Danh sách sản phẩm
              </Text>
            </View>

            <Animated.View style={[styles.fadingContainer, customStyles.searchContainer]}>
              <TextInput
                ref={this.refInput}
                style={styles.textInput}
                autoCapitalize="none"
                keyboardType="default"
                placeholderTextColor="gray"
                placeholder="Tìm kiếm sản phẩm"
              />
              <MIcon name="close" size={24} color="#636e72" onPress={this.fadeOut} />
            </Animated.View>
            <TouchableHighlight style={styles.iconTouchable}>
              <MIcon name="magnify" size={24} color="#636e72" onPress={this.fadeIn} />
            </TouchableHighlight>
          </View>

        </View>

        <TouchableHighlight activeOpacity={1} underlayColor="transparent" style={styles.container} onPress={this.fadeOut} children={<View />} />

      </View>
    );
  }
}
