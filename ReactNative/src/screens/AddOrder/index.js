import React from 'react';
import { View, TextInput, TouchableHighlight } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Components

// Variables
import styles from './styles';

export default class AddOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      maSKU: '',
      price: '',
      products: '',
      loading: false,
    });
  }
  render = () => {
    return (
      <View style={styles.container}>

        <View style={styles.viewDepartment}>
          <TouchableHighlight
            style={styles.iconTouchable}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <MIcon name="close" size={24} color="#636e72" />
          </TouchableHighlight>
          <View style={styles.textInputContainer}>
            <MIcon name="magnify" size={24} color="#636e72" />
            <TextInput
              autoCapitalize="none"
              keyboardType="default"
              placeholderTextColor="gray"
              placeholder="Tìm kiếm sản phẩm"
              style={styles.textInput}
              onChangeText={
                (text) => {
                  this.setState({ products: text });
                }
              }
              value={this.state.products}
            />
          </View>
          <TouchableHighlight style={styles.iconTouchable}>
            <MIcon name="barcode-scan" size={24} color="#636e72" />
          </TouchableHighlight>
        </View>
      </View >
    );
  }
}
