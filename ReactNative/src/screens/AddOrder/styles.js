import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewDepartment: {
    borderWidth: 1,
    paddingVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },

  iconTouchable: {
    paddingHorizontal: 10,
  },

  textInputContainer: {
    flex: 1,
    borderRadius: 10,
    paddingVertical: 0,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  textInput: {
    flex: 1,
  },

});
