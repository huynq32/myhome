import React from 'react';
import { View, Text, ScrollView } from 'react-native';

// Components

// Variables
import styles from './styles';

export default class Financial extends React.Component {

  render = () => {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.viewGeneralMin}>
          <View style={styles.viewList}>
            <Text style={styles.textList}>Báo cáo tồn kho</Text>
          </View>
          <View style={styles.viewList}>
            <Text style={styles.textList}>Sổ kho</Text>
          </View>
          <View style={styles.viewList}>
            <Text style={styles.textList}>Xuất nhập tồn</Text>
          </View>
          <View style={styles.viewList}>
            <Text style={styles.textList}>Báo cáo dưới định mức</Text>
          </View>
          <View style={styles.viewList1}>
            <Text style={styles.textList}>Báo cáo vượt định mức</Text>
          </View>
          </View>
        </ScrollView>

      </View>
    );
  }
}
