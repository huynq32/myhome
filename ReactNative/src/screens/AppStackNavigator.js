import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import TabNavigator from './TabNavigator';
import OrderStackNavigator from './OrderStackNavigator';
import ProductStackNavigator from './ProductStackNavigator';
import HomeStackNavigator from './HomeStackNavigator';


const Stack = createStackNavigator();

export default () => (
  <Stack.Navigator>
    <Stack.Screen name="TabNavigator" component={TabNavigator} options={{ headerShown: false }} />
    <Stack.Screen name="HomeStackNavigator" component={HomeStackNavigator} options={{ headerShown: false }} />
    <Stack.Screen name="OrderStackNavigator" component={OrderStackNavigator} options={{ headerShown: false }} />
    <Stack.Screen name="ProductStackNavigator" component={ProductStackNavigator} options={{ headerShown: false }} />
  </Stack.Navigator>
);
