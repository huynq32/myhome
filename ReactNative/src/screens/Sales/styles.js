import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
  },

  netProfitT: {
    marginTop: 10,
    borderRadius: 10,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: Color.WHITE,
  },

  netProfit: {
    paddingVertical: 10,
    flexDirection:'row',
    borderBottomWidth: 1,
    borderColor: Color.GRAY,
  },

  netProfit1: {
    paddingVertical: 10,
    flexDirection:'row',
  },

  textnetProfit: {
    fontSize: 17,
    paddingHorizontal: 10,
    color: Color.DARKGRAY,
    textAlignVertical: 'center',
  },

  viewTextFieldT: {
    marginVertical: 15,
    alignItems: 'center',
    flexDirection: 'column',
  },

  viewTextField: {
    flex: 1,
    marginRight: 10,
  },

  viewTextField1: {
    flex: 1,
  },

  textData: {
    color: Color.TEXT,
    marginVertical: 10,
  },

});
