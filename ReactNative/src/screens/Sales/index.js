import React from 'react';
import { View, Text } from 'react-native';
// Components
import { TextField } from 'react-native-material-textfield';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Variables
import styles from './styles';
import { ScrollView } from 'react-native-gesture-handler';

export default class Financial extends React.Component {

  render = () => {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.netProfitT}>
          <View style={styles.netProfit}>
            <Text style={styles.textnetProfit}>THỰC THU</Text>
          </View>
          <View style={styles.netProfit1}>
            <View style={styles.viewTextField}>
              <TextField
                placeholder="0"
                label="Tổng chi"
                autoCapitalize="none"
                keyboardType="numeric"
                placeholderTextColor="black"
              />
            </View>
            <View style={styles.viewTextField1}>
              <TextField
                placeholder="0"
                label="Tổng thu"
                autoCapitalize="none"
                keyboardType="numeric"
                placeholderTextColor="black"
              />
            </View>
          </View>
        </View>

        <View style={styles.netProfitT}>
          <View style={styles.netProfit}>
            <Text style={styles.textnetProfit}>DOANH THU</Text>
          </View>
          <View style={styles.viewTextFieldT}>
            <MIcon name="finance" size={80} color="silver" />
            <Text style={styles.textData}>Chưa có dữ liệu</Text>
          </View>
        </View>

        <View style={styles.netProfitT}>
          <View style={styles.netProfit}>
            <Text style={styles.textnetProfit}>LỢI NHUẬN GỘP</Text>
          </View>
          <View style={styles.viewTextFieldT}>
            <MIcon name="finance" size={80} color="silver" />
            <Text style={styles.textData}>Chưa có dữ liệu</Text>
          </View>
        </View>

        <View style={styles.netProfitT}>
          <View style={styles.netProfit}>
            <Text style={styles.textnetProfit}>TOP HÀNG BÁN CHẠY</Text>
          </View>
          <View style={styles.viewTextFieldT}>
            <MIcon name="finance" size={80} color="silver" />
            <Text style={styles.textData}>Chưa có dữ liệu</Text>
          </View>
        </View>

      </ScrollView>
    );
  }
}
