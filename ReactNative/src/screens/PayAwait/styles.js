import { StyleSheet } from 'react-native';
import Color from '../../constants/Color';

export default StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },

  viewDepartment: {
    borderWidth: 1,
    paddingVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: Color.GRAY,
    backgroundColor: Color.WHITE,
  },

  iconTouchable: {
    paddingHorizontal: 10,
  },

  textInputContainer: {
    flex: 1,
    borderRadius: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    backgroundColor: Color.BACKGROUNDCOLOR,
  },
  viewBarcode: {
    width: 35,
    alignItems: 'flex-end',
    borderLeftWidth: 1,
    borderColor: Color.GRAY,
  },
});
