import React from 'react';
import { View, TextInput, TouchableHighlight } from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

// Components

// Variables
import styles from './styles';

export default class PayAwait extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      maSKU: '',
      price: '',
      products: '',
      discount: '',
      loading: false,
    });
  }


  render = () => {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>

        <View style={styles.viewDepartment}>
          <TouchableHighlight
            style={styles.iconTouchable}
            onPress={() => {
              navigation.goBack();
            }}>
            <MIcon name="arrow-left" size={24} color="#636e72" />
          </TouchableHighlight>
          <View style={styles.textInputContainer}>
            <TextInput
              autoCapitalize="none"
              keyboardType="default"
              placeholderTextColor="gray"
              placeholder="Nhập tên, số điện thoại , mã"
              style={styles.textInput}
              onChangeText={
                (text) => {
                  this.setState({ products: text });
                }
              }
              value={this.state.products}
            />
            <View style={styles.viewBarcode}>
              <MIcon name="barcode-scan" size={24} color="#636e72" />
            </View>
          </View>
          <TouchableHighlight style={styles.iconTouchable}>
            <MIcon name="magnify" size={24} color="#636e72" />
          </TouchableHighlight>
        </View>

      </View>
    );
  }
}
