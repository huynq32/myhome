import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  imageBackground: {
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 34 : 0,
  },
  view: {
    height: 64,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'flex-end',
  },
  textInput: {
    height: 40,
    margin: 10,
    width: 142,
    padding: 9,
    borderWidth: 1,
    color: 'green',
    borderColor: 'green',
  },
  touchableHighlight: {
    marginRight: 9,
  },
  img: {
    width: 35,
    height: 35,
  },
  flatlist: {
    flex: 1,
  },
  textContainer: {
    height: 40,
    margin: 10,
    width: 250,
    padding: 1,
    borderWidth: 1,
    color: 'green',
    alignSelf: 'center',
    flexDirection: 'row',
    borderColor: 'green',
    justifyContent: 'space-around',
  },
  text1: {
    color: 'red',
    fontSize: 18,
  },
  text2: {
    color: 'green',
    fontSize: 18,
  },
});

export default {
  ...styles,
};
