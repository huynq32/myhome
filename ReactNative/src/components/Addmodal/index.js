import React, { Component } from 'react';
import { Text } from 'react-native';
import Button from 'react-native-button';
import Modal from 'react-native-modalbox';
import { TextInput } from 'react-native-paper';
import FlatListdata from '../../test/TestProduct/ListProduct';
import styles from './styles';


export default class AddModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			newFoodName: '',
			newFoodDescription: '',
		};
	}
	showAddModal = () => {
		// eslint-disable-next-line react/no-string-refs
		this.refs.myModal.open();
	}
	generateKey = (numberOfCharacter) => {
		// return require('random-string')({ length: numberOfCharacter });
	}
	render() {
		return (
			<Modal
				// eslint-disable-next-line react/no-string-refs
				ref={'myModal'}
				style={styles.stylesModal}
				position="center"
				backdrop={true}
				onClosed={() => {
					//alert("Modal closed");
				}}
			>
				<Text style={styles.textModal}>
					Add new menu
        </Text>
				<TextInput
					style={styles.textInput}
					onChangeText={(text) => this.setState({ newFoodName: text })}
					placeholder="Enter new food name"
					value={this.state.newFoodName}
				/>
				<TextInput
					style={styles.textInput}
					onChangeText={(text) => this.setState({ newFoodDescription: text })}
					placeholder="Enter new food Description"
					value={this.state.newFoodDescription}
				/>
				<Button
					style={styles.styleButton}
					onPress={() => {
						// // eslint-disable-next-line eqeqeq
						// if (this.state.newFoodName.length == 0 || this.state.newFoodDescription.length == 0) {
						// 	// eslint-disable-next-line no-alert
						// 	alert('You must enter food name and des..');
						// 	return;
						// }
						// const newKey = this.generateKey(2);
						// const newFood = {
						// 	name: this.state.newFoodName,
						// 	description: this.state.newFoodDescription,
						// 	key: 'newKey',
						// 	imageUrl: 'https://www.visitsarasota.com/sites/default/files/styles/listing_node_full/public/mmg_lfef_images/img-academy-156-e1cc497311032f22eae4e66ce77b23f3.jpg?itok=A128LF3B',

						// };
						// FlatListdata.push(newFood);
						// this.props.parenFlatList.refreshFlatList(newKey);
						// // eslint-disable-next-line react/no-string-refs
						// this.refs.myModal.close();
					}}>
					Save
          </Button>
			</Modal>
		);
	}
}
