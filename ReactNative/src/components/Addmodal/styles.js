import { StyleSheet, Dimensions } from 'react-native';

var screen = Dimensions.get('window');
export default StyleSheet.create({
  stylesModal: {
    height: 280,
    borderRadius: 0,
    shadowRadius: 10,
    justifyContent: 'center',
    width: screen.width - 80,
  },
  textModal: {
    fontSize: 16,
    marginTop: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textInput: {
    height: 40,
    marginTop: 20,
    marginLeft: 30,
    marginRight: 30,
  },
  styleButton: {
    padding: 8,
    height: 48,
    marginLeft: 70,
    marginRight: 70,
    borderRadius: 6,
  },
});
