import { StyleSheet ,Platform } from 'react-native';

const styles = StyleSheet.create({
  imageBackground: {
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 34 : 0,
  },
  view: {
    height: 64,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'space-around',
  },
  textInput: {
    height: 40,
    margin: 10,
    width: 142,
    padding: 9,
    borderWidth: 1,
    color: 'green',
    borderColor: 'green',
  },
  touchableHighlight: {
    marginRight: 9,
  },
  img: {
    width: 35,
    height: 35,
  },
  flatlist: {
    flex: 1,
  },
  textContainer: {
    height: 40,
    margin: 10,
    width: 250,
    padding: 9,
    // borderWidth: 1,
    color: 'green',
    alignSelf: 'center',
    flexDirection: 'column',
    // borderColor: 'green',
    justifyContent: 'space-around',
  },
  text1: {
    color: 'red',
  },
  text2: {
    color: 'green',
  },
});

export default {
  ...styles,
};
