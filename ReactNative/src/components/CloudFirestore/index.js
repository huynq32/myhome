import React, { Component } from 'react';
import { View, Text, Button, TextInput, TouchableHighlight, Image, FlatList, ImageBackground, Platform } from 'react-native';
import ImagePicker from 'react-native-image-picker';

// Components
import firebase from 'react-native-firebase';
// Variables
import styles from './styles';
import { firebaseAxios } from '../../constants/api';

export default class CloudFirestore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filePath: {},
      cloudTasks: [],
      newName: '',
      newImage: '',
      newImageUri: '',
      loading: false,
    };
    this.ref = firebase.firestore().collection('cloudTasks');
  }
  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot((querySnapshot) => {
      const clouds = [];
      querySnapshot.forEach((doc) => {
        clouds.push({
          newName: doc.data().newName,
          newImage: doc.data().newImage,
        });
      });
      this.setState({
        cloudTasks: clouds,
        loading: false,
      });
    });
  }
  onPressAdd = async () => {
    const stateModel = {
      newImage: '',
      newImageUri: '',
      loading: true,
    };
    if (this.state.newName.trim() === '') {
      alert('Task name is blank');
      return;
    }
    // if (this.state.newImage.trim() === '') {
    //   alert('Task price is blank');
    //   return;
    // }

    // try {
    //   const response = await this.ref.add({
    //     newName: this.state.newName,
    //     newImage: this.state.newImage,
    //   });

    //   console.log(response);

    //   const document = await response.get();
    // } catch (error) {
    //   console.log(error);
    // }

    const { filePath } = this.state;

    const formData = new FormData();

    formData.append('newName', this.state.newName);
    formData.append('newImage', {
      name: filePath.fileName,
      type: filePath.type,
      uri: Platform.OS === 'android' ? filePath.uri : filePath.uri.replace('file://', ''),
    });

    try {
      const response = await firebaseAxios.post('/helloWorld', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        }
      });
      stateModel.newName = '';
    } catch (error) {
      console.log(error);
      // console.log(error.name);
      // console.log(error.message);
    }

    this.setState(stateModel);
  }

  keyExtractor = (item, index) => index.toString();

  // onChangeText = (text) => {
  //   const stateModel = { newImage: this.state.newImage };

  //   const notNumbericRegex = /\D+/gm;

  //   const isOnlyNumber = notNumbericRegex.test(text) === false;

  //   if (isOnlyNumber) {
  //     stateModel.newImage = text;
  //   }

  //   this.setState(stateModel);
  // }

  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        this.setState({
          filePath: source,
        });
      }
    });
  };
  render = () => {
    const { cloudTasks } = this.state;

    return (
      <ImageBackground
        source={{ uri: 'https://img.freepik.com/free-vector/abstract-background-with-watercolor-texture_1048-2144.jpg?size=338&ext=jpg' }}
        style={styles.imageBackground}>
        <View style={styles.container}>
          <View style={styles.view}>
            <TextInput style={styles.textInput}
              autoCapitalize="none"
              keyboardType="default"
              placeholderTextColor="green"
              placeholder="Enter task name"
              onChangeText={
                (text) => {
                  this.setState({ newName: text });
                }
              }
              value={this.state.newName}
            />

            <Button
              title="Choose File"
              onPress={this.chooseFile}
              value={this.state.newImage}
            />

            <TouchableHighlight
              style={styles.touchableHighlight}
              underlayColor="tomato"
              onPress={this.onPressAdd}
            >
              <Image
                style={styles.img}
                source={require('../../components/resources/icon/add_green.png')}
              />
            </TouchableHighlight>
          </View>
          <FlatList
            styles={styles.flatlist}
            data={cloudTasks}
            keyExtractor={this.keyExtractor}
            renderItem={({ item, index }) => {
              return (
                <React.Fragment>
                  <View style={styles.textContainer}>
                    <Text>{this.newName}</Text>
                  </View>
                </React.Fragment>
              );
            }}
          />
        </View>
      </ImageBackground>
    );
  }
}
