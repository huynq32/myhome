import React, { Component } from 'react';
import { Text } from 'react-native';
import styles from '../Addmodal/styles';
import Button from 'react-native-button';
import Modal from 'react-native-modalbox';
import { TextInput } from 'react-native-paper';
import FlatListdata from '../../test/TestProduct/ListProduct';

export default class EditModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			foodName: '',
			foodDescription: '',
		};
	}
	showEditModal = (editingFood, flatlistItem) => {
		this.setState({
			key: editingFood.key,
			foodName: editingFood.name,
			flatlistItem: flatlistItem,
			foodDescription: editingFood.foodDescription,
		});
		// eslint-disable-next-line react/no-string-refs
		this.refs.myModal.open();
	}
	render() {
		return (
			<Modal
				// eslint-disable-next-line react/no-string-refs
				ref={'myModal'}
				style={styles.stylesModal}
				position="center"
				backdrop={true}
				onClosed={() => {
				}}
			>
				<Text style={styles.textModal}>
					Edit new menu
        </Text>
				<TextInput
					style={styles.textInput}
					onChangeText={(text) => this.setState({ foodName: text })}
					placeholder="Enter edit food name"
					value={this.state.foodName}
				/>
				<TextInput
					style={styles.textInput}
					onChangeText={(text) => this.setState({ foodDescription: text })}
					placeholder="Enter edit food Description"
					value={this.state.foodDescription}
				/>
				<Button
					style={styles.styleButton}
					onPress={() => {
						// eslint-disable-next-line eqeqeq
						if (this.state.foodName.length == 0 || this.state.foodDescription.length == 0) {
							// eslint-disable-next-line no-alert
							alert('You must enter food name and des..');
							return;
						}
						//Update food
						// eslint-disable-next-line eqeqeq
						var foundIndex = FlatListdata.findIndex(item => this.state.key == item.key);
						if (foundIndex < 0) {
							return;
						}
						FlatListdata(foundIndex).name = this.state.foodDescription;
						// eslint-disable-next-line react/no-string-refs
						this.refs.myModal.close();
					}}>
					Save
        </Button>
			</Modal>
		);
	}
}
