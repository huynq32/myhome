import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import axios from 'axios';

import styles from './styles';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: {},
    };
  }
  componentDidMount = async () => {
    try {
      const response = await axios.get('https://us-central1-dbonline-56d31.cloudfunctions.net/getStorageFiles');

      const { data: json } = response;

      this.setState({ list: json.data[0] });
    } catch (error) {
      console.log(error);
    }
  }
  render() {
    const { list } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header} />
        <Image style={styles.avatar} source={{ uri: list.imageUrl }} />
        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text style={styles.name}>{list.id}</Text>
            <Text style={styles.info}>{list.name}</Text>          
          </View>
        </View>
      </View>
    );
  }
}
