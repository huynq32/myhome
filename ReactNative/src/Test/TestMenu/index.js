import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

export default class Menu extends Component {
    render() {
        return (
            <View
                style={styles.container}>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('Products');
                    }}
                >
                    <Text style={styles.textTou}>Products</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('User');
                    }}
                >
                    <Text style={styles.textTou}>Users</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.goBack();
                    }}
                >
                    <Text style={styles.textTou1}>Back</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
