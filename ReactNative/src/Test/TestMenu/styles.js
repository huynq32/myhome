import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textTou: {
    padding: 10,
    fontSize: 15,
    color: 'green',
  },
  textTou1: {
    padding: 10,
    fontSize: 20,
    color: 'red',
  },
});
