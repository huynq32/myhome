// eslint-disable-next-line no-unused-vars
import React, { Component } from 'react';


export default class Data extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flatListItems: [
        { name: 'A', key: '1' }, { name: 'B', key: '2' }, { name: 'C', key: '3' },
        { name: 'D', key: '4' }, { name: 'E', key: '5' }, { name: 'F', key: '6' },
        { name: 'G', key: '7' }, { name: 'H', key: '8' }, { name: 'I', key: '9' },
        { name: 'K', key: '10' }, { name: 'L', key: '11' }, { name: 'M', key: '12' },
        { name: 'N', key: '13' }, { name: 'O', key: '14' }, { name: 'Y', key: '15' },
      ],
    };
  }
}
