import React, { Component } from 'react';
import { FlatList, View, Text, Image } from 'react-native';
import axios from 'axios';

import styles from './styles';


class FlatListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRowKey: null,
    };
  }
  render() {
    const { item } = this.props;
    return (
        <View style={styles.containerFlatListItem}>
          <Image
            source={{ uri: item.imageUrl }}
            style={styles.imageFlatListItem} />
          <View style={styles.viewContainer}>
            <Text style={styles.textContainer}>{item.name}</Text>
            <Text style={styles.textContainer}>{item.description}</Text>
          </View>
        </View>
    );
  }
}



export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myData: [],
    };
  }
  componentDidMount = async () => {
    try {
      const response = await axios.get('https://picsum.photos/v2/list?page=2');

      let { data: json } = response;

      json = json.map((item, index) => ({
        key: index.toString(),
        name: item.author,
        description: item.url,
        imageUrl: item.download_url,
      }));
      console.log('data: ', json);

      this.setState({ myData: json });

    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { myData } = this.state;
    return (
      <View>
      <FlatList
        data={myData}
        renderItem={({ item, index }) => {
          return (
            <FlatListItem
              item={item}
              index={index}
              parenFlatList={this} />);
        }}
      />
      </View >
    );
  }
}
