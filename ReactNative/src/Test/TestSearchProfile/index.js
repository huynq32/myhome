import React, { Component } from 'react';
import { Text, View, Image, Alert, TouchableOpacity } from 'react-native';
import axios from 'axios';

import styles from './styles';
import { Button } from 'react-native-elements';
import { TextInput } from 'react-native-paper';
import Slider from '@react-native-community/slider';
import { Switch } from 'react-native-gesture-handler';

export default class SearchProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
    };
  }
  componentDidMount = async () => {
    try {
      const response = await axios.get('https://picsum.photos/v2/list?page=2&limit=100');

      const { data } = response;
      //profile: data[...]
      this.setState({ profile: data[80] });
    } catch (error) {
      console.log(error);
    }
  }
  buttonClickListener = () => {
    Alert.alert('Thong Bao: Ban da nhan vao click me');
  }

  render() {
    const { profile } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header} />
        <Image style={styles.avatar} source={{ uri: profile.download_url }} />
        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text style={styles.name}>{profile.author}</Text>
            <Text style={styles.info}>UX Designer / Mobile developer</Text>
            <Text style={styles.description}>Fresesh</Text>
            <TouchableOpacity style={styles.buttonContainer}>
              <View><Text>{profile.height}</Text></View>
              <View style={styles.itemTou}><Switch style={styles.switchView} /></View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainer}>
              <View><Text>{profile.width}</Text></View>
              <View style={styles.itemTou}><Slider style={styles.sliderView} /></View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainer}>
              <View><Text>Option 3</Text></View>
              <View style={styles.itemTou}><TextInput style={styles.item} /></View>
            </TouchableOpacity>
            <View>
              <Button
                onPress={this.buttonClickListener}
                title="Click ME"
                color="#00B0FF"
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}
