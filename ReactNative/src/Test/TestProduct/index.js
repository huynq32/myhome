import React, { Component } from 'react';
import { FlatList, Text, View, Alert, Image, TouchableHighlight } from 'react-native';
import axios from 'axios';

import styles from './styles';
import FlatListdata from './ListProduct';
import AddModal from '../../components/Addmodal';
import EditModal from '../../components/EditModal';
import LongPressForAndroidSwipeout  from 'react-native-swipeout-longpressforandroid';


class FlatListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRowKey: null,
    };
  }
  render() {
    const sw = {
      autoClose: true,
      onClose: () => {
        if (this.state.activeRowKey != null) {
          this.setState({ activeRowKey: null });
        }
      },
      onOpen: (sectionId, rowId, direction) => {
        this.setState({ activeRowKey: this.props.item.key });
      },
      right: [
        {
          onPress: () => {
            //alert('Update');
            this.props.parenFlatList.refs.EditModal.showEditModal(FlatListdata[this.props.index], this);
          },
          text: 'Edit', type: 'primary',
        },
        {
          onPress: () => {
            const deletingRow = this.state.activeRowKey;
            Alert.alert(
              'Alert',
              'Are you sure you want to delete ?',
              [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                  text: 'Yes', onPress: () => {
                    FlatListdata.splice(this.props.index, 1);
                    //Refresh FlatList
                    this.props.parenFlatList.refreshFlatList(deletingRow);
                  },
                },
              ],
              { cancelable: true }
            );
          },
          text: 'Delete', type: 'delete',
        },
      ],
      rowId: this.props.index,
      sectionId: 1,
    };
    const { item } = this.props;
    return (
      <LongPressForAndroidSwipeout  {...sw}>
        <View style={styles.containerFlatListItem}>
          <Image
            source={{ uri: item.imageUrl }}
            style={styles.imageFlatListItem} />
          <View style={styles.viewContainer}>
            <Text style={styles.textContainer}>{item.name}</Text>
            {/* <Text style={styles.textContainer}>{item.description}</Text> */}
          </View>
        </View>
      </LongPressForAndroidSwipeout >
    );
  }
}

export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteRowKey: null,
      myData: [],
    };
  }

  componentDidMount = async () => {

    try {
      const response = await axios.get('https://us-central1-dbonline-56d31.cloudfunctions.net/getStorageFiles');
      // console.log("Product -> componentDidMount -> response", response)

      let { data: json } = response;

      json = json.data.map((item, index) => ({
        key: index.toString(),
        name: item.name,
        imageUrl: item.imageUrl,
      }));

      this.setState({ myData: json });

    } catch (error) {
      console.log(error);
    }
  }

  refreshFlatList = (activeKey) => {
    this.setState((prevState) => {
      return {
        deleteRowKey: activeKey,
      };
    });
    // eslint-disable-next-line react/no-string-refs
    this.refs.flatList.scrollToEnd();
  }

  onPressAdd = () => {
    // eslint-disable-next-line react/no-string-refs
    this.refs.addModal.showAddModal();
  }

  render() {
    const { myData } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.containerView2}>
          <TouchableHighlight
            style={styles.touchableHighlight}
            underlayColor="tomato"
            onPress={this.onPressAdd}>
            <Image
              style={styles.imageTou}
              source={require('../../components/resources/icon/add_green.png')}
            />
          </TouchableHighlight>
        </View>
        <FlatList
          // eslint-disable-next-line react/no-string-refs
          ref={'flatList'}
          data={myData}
          renderItem={({ item, index }) => {
            return (
              <FlatListItem
                item={item}
                index={index}
                parenFlatList={this} />);
          }}
        />

        <AddModal
          // eslint-disable-next-line react/no-string-refs
          ref={'addModal'}
          parenFlatList={this} />
        <EditModal
          // eslint-disable-next-line react/no-string-refs
          ref={'EditModal'}
          parenFlatList={this} />
      </View>
    );
  }
}
