import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  containerFlatListItem: {
    flex: 1,
    padding:5,
    borderWidth: 1,
    borderColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  imageFlatListItem: {
    width: 100,
    height: 100,
    borderRadius: 10,
    alignSelf: 'center',
  },
  viewContainer: {
    flex: 1,
  },
  textContainer: {
    fontSize: 16,
    color: 'red',
    marginTop:0,
    marginLeft: 15,
    marginRight: 10,
  },
  container: {
    flex: 1,
    marginTop: 0,
  },
  containerView2: {
    height: 64,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'flex-end',
  },
  touchableHighlight: {
    marginRight: 10,
  },
  imageTou: {
    width: 35,
    height: 35,
  },
});
