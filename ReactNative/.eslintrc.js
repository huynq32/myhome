module.exports = {
  env: {
    es6: true
  },
  root: true,
  extends: '@react-native-community',
  rules: {
    "prettier/prettier": "off",
    "no-unused-vars": "error",
    "semi": "error",
    "comma-dangle": "error",
    "eol-last": "error",
    "quotes": "error",
    "react-native/no-inline-styles": "error",
    "no-spaced-func": "error",
    "keyword-spacing": "error",
    "space-infix-ops": "error",
    "react/no-did-mount-set-state": "off",
  },
};
