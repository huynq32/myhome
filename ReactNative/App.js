// import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';

// Components
import AppContainer from './src/screens/AppContainer';

//Variables
import store from './src/redux/store';

const App = () => (
  <Provider store={store}>
    <AppContainer />
  </Provider>
);

export default App;
