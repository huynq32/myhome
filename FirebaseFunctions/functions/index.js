const admin = require('firebase-admin');
const functions = require('firebase-functions');

const os = require('os');
const fs = require('fs');
const path = require('path');
const Busboy = require('busboy');
const uuidv4 = require('uuid').v4;

const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  storageBucket: 'dbonline-56d31.appspot.com',
  credential: admin.credential.cert(serviceAccount),
});
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const storage = admin.storage();
const bucket = storage.bucket();
const firestore = admin.firestore();

exports.helloWorld = functions.https.onRequest(async (request, response) => {
  // console.log(request.body);

  console.log(request.method.toUpperCase());

  const jsonModel = {
    status: 'SUCCESS',
    fields: {},
    files: [],
  };

  const tmpdir = os.tmpdir();

  const uploadFilePromises = [];

  const busboy = new Busboy({ headers: request.headers });


  // Sự kiện này kích hoạt khi trong data gửi lên có trường KHÔNG PHẢI LÀ FILE (non-file)
  busboy.on('field', (fieldname, value) => {
    console.log(`Processed field ${fieldname}: ${value}`);
    jsonModel.fields[fieldname] = value;
  });

  // Sự kiện này kích hoạt khi trong data gửi lên có trường là FILE
  busboy.on('file', (fieldname, file, filename) => {
    console.log(`Processed file ${filename}`);

    const filepath = path.join(tmpdir, filename);

    const writeStream = fs.createWriteStream(filepath);

    file.pipe(writeStream);

    const promise = new Promise((resolve, reject) => {
      file.on('end', () => {
        writeStream.end();
      });
      writeStream.on('finish', async (a) => {
        try {
          const firebaseStorageDownloadTokens = uuidv4();

          const destination = `uploads/${Date.now()}_${filename}`;
          /**
           * Đợi upload lên firebase storage để xóa file tạm filepath
           */
          await bucket.upload(filepath, {
            destination,
            metadata: {
              metadata: {
                firebaseStorageDownloadTokens
              }
            }
          });

          // const url = `https://firebasestorage.googleapis.com/v0/b/dbonline-56d31.appspot.com/o/uploads%2Fimage-ac3dba84-aefb-44d2-ab8e-bf4718091e96.jpg?alt=media&token=734cbba0-fcb1-4206-aa82-e23e08ec0f07`
          const storageUrl = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${encodeURIComponent(destination)}?alt=media&token=${firebaseStorageDownloadTokens}`;

          console.log(storageUrl);

          jsonModel.files.push({ filename, filepath, storageUrl });

          console.log('upload to storage response success');
        } catch (error) {
          console.log('upload to storage fail');
          console.log(error);
        }

        resolve();
      });
      writeStream.on('error', reject);
    });

    uploadFilePromises.push(promise);
  });

  // Sự kiện này kích hoạt khi mọi trường dữ liệu (file/non-file) đã nhận được từ server
  busboy.on('finish', async () => {
    await Promise.all(uploadFilePromises);

    const documentModel = {
      imageUrl: '',
      name: jsonModel.fields.newName,
    };

    for (const file of jsonModel.files) {
      fs.unlinkSync(file.filepath);
      documentModel.imageUrl = file.storageUrl;
    }

    await firestore.collection('cloudTasks').add(documentModel);

    response.status(200).send(jsonModel).end();
  });

  busboy.end(request.rawBody);

});

exports.getStorageFiles = functions.https.onRequest(async (request, response) => {
  const jsonModel = {
    status: 'SUCCESS',
    data: []
  };

  const data = await firestore.collection('cloudTasks').get();

  data.docs.forEach(item => {
    jsonModel.data.push({ ...item.data(), id: item.id });
  });

  response.status(200).json(jsonModel).end();
});
